import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


abstract public class BaseTest {

    protected WebDriver driver = null;
    protected static final String BASE_URL = "https://intra.t-systems.ru/Dash";
    public static WebDriverWait wait;
    public static Actions action;


    @BeforeMethod
    void openBrowser() {
        WebDriverManager.chromedriver().setup();
        initChrome();
        driver.get(BASE_URL);
    }

    @AfterMethod
    void closeBrowser() {
        driver.manage().deleteAllCookies();
        quitDriver();
    }

    protected void initChrome() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--ignore-certificate-errors");
        driver = new ChromeDriver(options);
        action = new Actions(driver);
    }


    protected void initMozila() {
        DesiredCapabilities capability = DesiredCapabilities.firefox();
        capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        driver = new FirefoxDriver(capability);

    }


    protected void quitDriver() {
        driver.quit();
    }


    protected void addDelay(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
