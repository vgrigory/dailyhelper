import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.xpath;

public class MbHelper {
    private WebDriver driver;
    private static WebDriverWait wait;

    public MbHelper(WebDriver driver) {
        this.driver = driver;
    }


    public MbHelper logInToIntra(String login, String password) {
        wait = new WebDriverWait(driver,
                TimeUnit.SECONDS.toSeconds(30),
                TimeUnit.SECONDS.toMillis(1));
        WebElement username = driver.findElement(By.id("UserName"));
        username.clear();
        username.sendKeys(login);
        WebElement pswrd = driver.findElement(By.id("Password"));
        pswrd.clear();
        pswrd.sendKeys(password);
        WebElement button = driver.findElement(By.id("logonButton"));
        button.click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(@data-bind,'text:prnresources.more')]")));
        return this;
    }

    public MbHelper submitThanks() {
        driver.findElement(By.xpath("//input[@class='postNewsButton btn']")).click();
        return this;
    }

    public MbHelper enterThanks(String reason) {
        driver.findElement(By.xpath("//div[@class='validationMessage text-error']/preceding-sibling::textarea")).click();
        driver.findElement(By.xpath("//div[@class='validationMessage text-error']/preceding-sibling::textarea")).sendKeys(reason);
        System.out.println("Thanks are entered: " + reason);
        return this;
    }

    public MbHelper selectReason() {
        driver.findElement(By.xpath("//a[@class='hmanyandforwhat text-error']")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='ko-popover']")));
        driver.findElement(By.xpath("(//label[@class='checkbox']//span[@data-bind])[1]")).click();
        driver.findElement(By.xpath("//button[@class='btn']")).click();
        return this;
    }

    public MbHelper clickAndEnterColleagues(String fileName) {
        String[] colleagues = getColleagues(fileName);

        for (String colleague : colleagues
        ) {
            selectEmployeeInSearchFieldOnMbPage(colleague);
        }
        return this;
    }

    public static void selectEmployeeInSearchFieldOnMbPage(String employeeName) {
        WebElement search = findXPath("//input[@id='s2id_autogen2']");
        search.sendKeys(employeeName);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='select2-result-label']")));
        findXPath("//div[@class='select2-result-label']").click();
    }

    public static WebElement findXPath(String xpath, Object... arsg) {
        String completeXpath = String.format(xpath, arsg);
        return wait.until(ExpectedConditions.presenceOfElementLocated(xpath(completeXpath)));
    }

    private String[] getColleagues(String fileName) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String str = null;
        ArrayList<String> list = new ArrayList<>();
        while (true) {
            try {
                if (reader != null && ((str = reader.readLine()) == null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (str != null && !str.isEmpty()) {
                list.add(str);
                System.out.println(str);
            }
        }
        return list.toArray(new String[0]);
    }

    public MbHelper clickOnMoreAndMb() {
        driver.findElement(By.xpath("//span[contains(@data-bind,'text:prnresources.more')]")).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='tabs']/i[@class='icon-givecake']")));
        driver.findElement(By.xpath("//a[@class='tabs']/i[@class='icon-givecake']")).click();
        return this;
    }


}
