import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HRSimLoginPage;

import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElement;

public class HrSimulatorTest {
    protected WebDriver driver = null;
    protected static final String BASE_URL = "https://10.91.5.30:1443/ciam.hrsim/#/login";
    public static WebDriverWait wait;
    public static Actions action;

    @BeforeMethod
    void openBrowser() {
        WebDriverManager.chromedriver().setup();
        initChrome();
        wait = new WebDriverWait(driver,
                TimeUnit.SECONDS.toSeconds(30),
                TimeUnit.SECONDS.toMillis(1));
        driver.get(BASE_URL);
    }

    @AfterMethod
    void closeBrowser() {
        driver.manage().deleteAllCookies();
        quitDriver();
    }


    @Test
    public void logInToSimulator() {
        driver.findElement(By.id("emailInput")).sendKeys(System.getenv("hrsim_login"));
        driver.findElement(By.id("passwordInput")).sendKeys(System.getenv("hrsim_password"));
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Assert.assertTrue(isSuccessfullyLoggedIn(), "Page is not opened");
    }

    @Test
    public void logInToSimulator_1() {
        var hrSimLoginPage = new HRSimLoginPage(driver);
        hrSimLoginPage.getUserName().sendKeys(System.getenv("hrsim_login"));
        hrSimLoginPage.getPassword().sendKeys("12345");
        hrSimLoginPage.getLogInButton().click();
        Assert.assertTrue(isSuccessfullyLoggedIn(), "Page is not opened");
    }

    protected void initChrome() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.addArguments("--ignore-certificate-errors");
        driver = new ChromeDriver(options);
        action = new Actions(driver);
    }

    protected void quitDriver() {
        driver.quit();
    }

    public boolean isSuccessfullyLoggedIn() {
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2")));
            return doesPageContain(By.xpath("//h2"), "Welcome to HR-Simulator!", wait);
        } catch (TimeoutException e) {
            return false;
        }
    }

    protected boolean doesPageContain(By locator, String text, WebDriverWait wait) {
        try {
            WebElement element = driver.findElement(locator);
            wait.until(textToBePresentInElement(element, text));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

}
