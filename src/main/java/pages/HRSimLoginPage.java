package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HRSimLoginPage {
    protected WebDriver driver;
    private static final By USERNAME_ID = By.id("emailInput");
    private static final By PASSWORD_ID = By.id("passwordInput");
    private static final By BUTTON_LOG_IN_ID = By.xpath("//button[@type='submit']");

    public HRSimLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getUserName() {
        return driver.findElement(USERNAME_ID);
    }


    public WebElement getPassword() {
        return driver.findElement(PASSWORD_ID);
    }


    public WebElement getLogInButton() {
        return driver.findElement(BUTTON_LOG_IN_ID);
    }

}
