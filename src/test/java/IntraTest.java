import org.testng.annotations.Test;

public class IntraTest extends BaseTest {
    @Test
    public void testIntraMb() {
        var mbHelper = new MbHelper(driver);
        mbHelper.logInToIntra(System.getenv("login"), System.getenv("intra_pswd"))
                .clickOnMoreAndMb()
                .clickAndEnterColleagues("originalList.txt")
                .selectReason()
                .enterThanks(";)")
                .submitThanks();
    }
}
